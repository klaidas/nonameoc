//
//  Action.m
//  noname
//
//  Created by klaidas on 28/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "Action.h"


@implementation Action

@dynamic action;
@dynamic eventID;
@dynamic id;
@dynamic userID;

@end
