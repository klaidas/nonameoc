//
//  Action.h
//  noname
//
//  Created by klaidas on 28/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Action : NSManagedObject

@property (nonatomic, retain) NSNumber * action;
@property (nonatomic, retain) NSNumber * eventID;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSNumber * userID;

@end
