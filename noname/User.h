//
//  User.h
//  noname
//
//  Created by klaidas on 15/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface User : NSManagedObject

@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * password;

@end
