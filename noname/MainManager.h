//
//  MainManager.h
//  noname
//
//  Created by klaidas on 15/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface MainManager : NSObject

+ (instancetype)sharedManager;
- (BOOL)internetConnectionAvaivable;
- (void)resetManagedObjectContext;
- (NSArray *)getEvents;
- (int)getUserIDForUserInfo:(NSDictionary *)dictionary;
- (BOOL)submitUserSearchSettings:(NSDictionary *)dictionary;


@end
