//
//  EventDetailsViewController.m
//  noname
//
//  Created by klaidas on 28/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "MainManager.h"

@interface EventDetailsViewController ()

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITextField *comment;
@property (weak, nonatomic) IBOutlet UISegmentedControl *typePicker;
@property (strong, nonatomic) MainManager *mainManager;

@end

@implementation EventDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.datePicker setMinimumDate:[NSDate date]];
    self.mainManager = [MainManager sharedManager];
}

- (IBAction)createAction:(UIButton *)sender
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    [dictionary setObject:@"2" forKey:@"operation"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"username"] forKey:@"username"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"password"] forKey:@"password"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"userID"] forKey:@"userID"];
    [dictionary setObject:[NSNumber numberWithDouble:self.coordinate.latitude] forKey:@"latitude"];
    [dictionary setObject:[NSNumber numberWithDouble:self.coordinate.longitude] forKey:@"longitude"];
    [dictionary setObject:[self.comment text] forKey:@"comment"];
    
    [dictionary setObject:[self.datePicker date] forKey:@"date"];
    if ([self.typePicker selectedSegmentIndex] == 0)
    {
        [dictionary setObject:@"road" forKey:@"type"];
    }
    else
    {
        [dictionary setObject:@"offroad" forKey:@"type"];
    }

    NSLog(@"%@", dictionary);
//    if ([self.mainManager createEvent:dictionary])
//    {
//        //successs
//        [self.navigationController popViewControllerAnimated:YES];
//        [self.tabBarController setSelectedIndex:0];
//    }
//    else
//    {
//        //error popup
//    }

}



@end
