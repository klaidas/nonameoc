//
//  TimeManager.m
//  noname
//
//  Created by klaidas on 23/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "TimeManager.h"

@implementation TimeManager

static float const MINUTE = 60;
static float const HOUR = MINUTE * 60;
static float const DAY = HOUR * 24;
static float const WEEK = DAY * 7;
static float const MONTH = WEEK * 4;

- (NSDate *)NSDateFromMySQLDate:(NSString *)mySQLDateTime
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    return [formatter dateFromString:mySQLDateTime];
}

- (NSString *)timeLeftTillDateTime:(NSDate *)dateTime
{
    if ([dateTime timeIntervalSinceNow] > 0)
    {
        return [NSString stringWithFormat:@"Starts in: %@",[self timeDescriptionFromInterval:[dateTime timeIntervalSinceNow]]];
    }
    else if ([dateTime timeIntervalSinceNow] < 0)
    {
        return [NSString stringWithFormat:@"Started %@ ago",[self timeDescriptionFromInterval:[dateTime timeIntervalSinceNow] * (-1)]];
    }
    else
    {
        return @"Starting Now!";
    }
}

- (NSString *)GMTDateInCurrentTimeZone:(NSDate *)datetime
{
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"latitude"] doubleValue]
                                                      longitude:[[[NSUserDefaults standardUserDefaults] objectForKey:@"longitude"] doubleValue]];
    NSTimeZone *timeZone = [[APTimeZones sharedInstance] timeZoneWithLocation:location];
    NSDate *dateTimeInCurrentTimeZone = [datetime dateByAddingTimeInterval:[timeZone secondsFromGMT]];
   
    return dateTimeInCurrentTimeZone.description;
}

- (NSString *)timeDescriptionFromInterval:(float)interval
{
    if (interval > MONTH)
    {
        return @"more than a month";
    }
    //week
    else if (interval > 3 * WEEK)
    {
        return @"3 weeks";
    }
    else if (interval > 2 * WEEK)
    {
        return @"2 weeks";
    }
    else if (interval > WEEK)
    {
        return @"1 week";
    }
    //day
    else if (interval > 6 * DAY)
    {
        return @"6 days";
    }
    else if (interval > 5 * DAY)
    {
        return @"5 days";
    }
    else if (interval > 4 * DAY)
    {
        return @"4 days";
    }
    else if (interval > 3 * DAY)
    {
        return @"3 days";
    }
    else if (interval > 2 * DAY)
    {
        return @"2 days";
    }
    else if (interval > 1 * DAY)
    {
        return @"1 days";
    }
    //hour
    else if (interval > 23 * HOUR)
    {
        return @"23 hours";
    }
    else if (interval > 22 * HOUR)
    {
        return @"22 hours";
    }
    else if (interval > 21 * HOUR)
    {
        return @"21 hours";
    }
    else if (interval > 20 * HOUR)
    {
        return @"20 hours";
    }
    else if (interval > 19 * HOUR)
    {
        return @"19 hours";
    }
    else if (interval > 18 * HOUR)
    {
        return @"18 hours";
    }
    else if (interval > 17 * HOUR)
    {
        return @"17 hours";
    }
    else if (interval > 16 * HOUR)
    {
        return @"16 hours";
    }
    else if (interval > 15 * HOUR)
    {
        return @"15 hours";
    }
    else if (interval > 14 * HOUR)
    {
        return @"14 hours";
    }
    else if (interval > 13 * HOUR)
    {
        return @"13 hours";
    }
    else if (interval > 12 * HOUR)
    {
        return @"12 hours";
    }
    else if (interval > 11 * HOUR)
    {
        return @"11 hours";
    }
    else if (interval > 10 * HOUR)
    {
        return @"10 hours";
    }
    else if (interval > 9 * HOUR)
    {
        return @"9 hours";
    }
    else if (interval > 8 * HOUR)
    {
        return @"8 hours";
    }
    else if (interval > 7 * HOUR)
    {
        return @"7 hours";
    }
    else if (interval > 6 * HOUR)
    {
        return @"6 hours";
    }
    else if (interval > 5 * HOUR)
    {
        return @"5 hours";
    }
    else if (interval > 4 * HOUR)
    {
        return @"4 hours";
    }
    else if (interval > 3 * HOUR)
    {
        return @"3 hours";
    }
    else if (interval > 2 * HOUR)
    {
        return @"2 hours";
    }
    else if (interval > 1 * HOUR)
    {
        return @"1 hour";
    }
    //minutes
    else if (interval > 50 * MINUTE)
    {
        return @"50 minutes";
    }
    else if (interval > 40 * MINUTE)
    {
        return @"40 minutes";
    }
    else if (interval > 30 * MINUTE)
    {
        return @"30 minutes";
    }
    else if (interval > 20 * MINUTE)
    {
        return @"20 minutes";
    }
    else if (interval > 10 * MINUTE)
    {
        return @"10 minutes";
    }
    else if (interval > 5 * MINUTE)
    {
        return @"5 minutes";
    }
    
    
    return @"";
}

@end
