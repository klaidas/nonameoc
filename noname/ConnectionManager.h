//
//  ConnectionManager.h
//  noname
//
//  Created by klaidas on 15/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManager : NSObject

- (NSDictionary *)formRequestForAllEvents;
- (id)performRequestWith:(NSDictionary *)dictionary responseIsJson:(BOOL)responseIsJson;


@end
