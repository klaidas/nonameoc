//
//  SearchSettingsViewController.m
//  noname
//
//  Created by klaidas on 17/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "SearchSettingsViewController.h"
#import "MainManager.h"

@interface SearchSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITextField *searchRangeTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *searchTypeSegmentController;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation SearchSettingsViewController

- (void)viewWillAppear:(BOOL)animated
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedin"] isEqualToString:@"YES"])
    {
        [self.saveButton setEnabled:YES];
        [self.searchRangeTextField setText:[[NSUserDefaults standardUserDefaults] objectForKey:@"searchRange"]];
    }
    else
    {
        [self.saveButton setEnabled:NO];
    }
}

- (IBAction)saveAction:(UIButton *)sender
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    [dictionary setObject:self.searchRangeTextField.text forKey:@"searchRange"];
    [dictionary setObject:[self.searchTypeSegmentController titleForSegmentAtIndex:self.searchTypeSegmentController.selectedSegmentIndex] forKey:@"searchType"];
    if ([[MainManager sharedManager] submitUserSearchSettings:dictionary])
    {
        [[NSUserDefaults standardUserDefaults] setObject:self.searchRangeTextField.text forKey:@"searchRange"];
        [[NSUserDefaults standardUserDefaults] setObject:[self.searchTypeSegmentController titleForSegmentAtIndex:self.searchTypeSegmentController.selectedSegmentIndex] forKey:@"searchType"];
        [[MainManager sharedManager] resetManagedObjectContext];
        NSLog(@"Settigns uploaded!");
    }
    else
    {
        NSLog(@"Settigns upload FAILES");
    }
}

@end
