//
//  EventSelectLocationViewController.m
//  noname
//
//  Created by klaidas on 28/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "EventSelectLocationViewController.h"
#import "MyMapView.h"
#import "EventDetailsViewController.h"

@interface EventSelectLocationViewController ()<CLLocationManagerDelegate, GMSMapViewDelegate, UITabBarControllerDelegate>

@property (weak, nonatomic) IBOutlet MyMapView *mapView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectButton;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property CLLocationCoordinate2D pickedLocation;

@end

@implementation EventSelectLocationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.mapView.delegate = self;
    [self setupLocationManager];
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    self.selectButton.enabled = NO;
    self.tabBarController.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"loggedin"] isEqualToString:@"NO"] || ![[NSUserDefaults standardUserDefaults] objectForKey:@"loggedin"])
    {
        [self.selectButton setEnabled:NO];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:@"Only logged in users can create events."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}



- (void)setupLocationManager {
    _locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    //[self.locationManager requestAlwaysAuthorization];
    [self.locationManager setDistanceFilter:kCLLocationAccuracyHundredMeters];
    
    [self.locationManager startUpdatingLocation];
    
}

- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.pickedLocation = coordinate;
    [self.mapView clear];
    GMSMarker *marker = [GMSMarker new];
    marker.position = coordinate;
    marker.map = self.mapView;
    if (![self.selectButton isEnabled])
    {
        [self.selectButton setEnabled:YES];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"createEvent"]) {
        if ([segue.destinationViewController isKindOfClass:[EventDetailsViewController class]]) {
            EventDetailsViewController *eventInfoVC = segue.destinationViewController;
            [eventInfoVC setCoordinate:self.pickedLocation];
        }
    }
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    [self.selectButton setEnabled:NO];
    [self.mapView clear];
}


@end
