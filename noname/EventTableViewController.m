//
//  EventTableViewController.m
//  noname
//
//  Created by klaidas on 02/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "EventTableViewController.h"
#import "MainManager.h"
#import "Event.h"
#import "MyMapView.h"
#import "EventCell.h"
#import "EventInfoViewController.h"
#import "TimeManager.h"


@interface EventTableViewController ()<UITableViewDelegate>


@property (weak, nonatomic) IBOutlet MyMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) MainManager *mainManager;
@property (strong, nonatomic) NSArray *eventsList;

@end

@implementation EventTableViewController

- (void) viewWillAppear:(BOOL)animated
{
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        self.eventsList = [self.mainManager getEvents];
        dispatch_async(dispatch_get_main_queue(), ^(void){
            [self updateEventsList];
        });
    });
    

}

//move shit to nib
- (void)viewDidLoad {
    [super viewDidLoad];
    self.mainManager  = [MainManager sharedManager];
    [self setupTableView];
    [self setupMapView];
}

- (void)setupMapView
{
    self.mapView.myLocationEnabled = YES;
}

- (void)updateEventsList
{
    [self.mapView setMapEvents:self.eventsList];
    [self.tableView reloadData];
    if (![self.eventsList count])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Info"
                                                        message:@"0 Events found, check your search settings."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }
}

#pragma mark - Table View
- (void)setupTableView
{
    UINib *nib = [UINib nibWithNibName:@"EventCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"eventCell"];
    UILongPressGestureRecognizer *longPressGestureRecognize = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGuestureOnTable:)];
    [self.tableView addGestureRecognizer:longPressGestureRecognize];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.eventsList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
    Event *event = [self.eventsList objectAtIndex:indexPath.row];
    TimeManager *timeManager = [TimeManager new];
    
    cell.type.text = [event type];
    cell.time.text = [timeManager timeLeftTillDateTime:event.date];
    cell.comment.text = [event comment];
    [timeManager GMTDateInCurrentTimeZone:event.date];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.mapView changeCameraLocationToMarkerAtIndex:indexPath.row];
}

- (void)longPressGuestureOnTable:(UILongPressGestureRecognizer*)recognizer
{
    
    CGPoint location = [recognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:location];

    if ( recognizer.state == UIGestureRecognizerStateBegan && indexPath )
    {
        [self.mapView changeCameraLocationToMarkerAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"segueShowInfo" sender:[self.tableView cellForRowAtIndexPath:indexPath]];
    }

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueShowInfo"]) {
        if ([segue.destinationViewController isKindOfClass:[EventInfoViewController class]]) {
            EventInfoViewController *eventInfoVC = segue.destinationViewController;
            [eventInfoVC setEvent:[self.eventsList objectAtIndex:[self.tableView indexPathForCell:sender].row]];
        }
    }
}

@end
