//
//  MyMapView.m
//  noname
//
//  Created by klaidas on 03/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "MyMapView.h"
#import "Event.h"

@implementation MyMapView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.locationSelectMode = NO;
    }
    return self;
}

- (void)changeCameraLocationToMarkerAtIndex:(NSInteger)index
{
    Event *event = [[self mapEvents] objectAtIndex:index];
    CLLocationCoordinate2D coordinates = CLLocationCoordinate2DMake([[event lattitude] doubleValue], [[event longitude] doubleValue]);
    self.camera = [GMSCameraPosition cameraWithTarget:coordinates zoom:11];
}

- (void)setMapEvents:(NSArray *)mapEvents
{
    _mapEvents = mapEvents;
    self.camera = [self calculateCameraPosition];
    [self setupMarkers];
}

- (GMSCameraPosition *)calculateCameraPosition
{
    if ([self.mapEvents count] == 1) {
        return [GMSCameraPosition cameraWithTarget:[self getEventCoordinates:[[self mapEvents] firstObject]] zoom:13];
    }else
    {
        return nil;
    }
}

- (void)setupMarkers
{
    [self clear];
    for (Event *event in self.mapEvents) {
        GMSMarker *marker = [[GMSMarker alloc] init];
        marker.appearAnimation = kGMSMarkerAnimationPop;
        marker.position = [self getEventCoordinates:event];
        marker.title = [event comment];
        //marker.snippet = [event creator];
        marker.map = self;
        marker.icon = [self getMarkerIconForEvent:event];
    }
}

- (UIImage *)getMarkerIconForEvent:(Event *)event
{
    if ([[event type] isEqualToString:@"road"]) {
        return [GMSMarker markerImageWithColor:[UIColor blackColor]];
    }
    if ([[event type] isEqualToString:@"offroad"]) {
        return [GMSMarker markerImageWithColor:[UIColor greenColor]];
    }
    return nil;
}

- (CLLocationCoordinate2D)getEventCoordinates:(Event *)event
{
    return CLLocationCoordinate2DMake([[event lattitude] doubleValue], [[event longitude] doubleValue]);
}


@end
