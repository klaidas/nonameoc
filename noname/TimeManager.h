//
//  TimeManager.h
//  noname
//
//  Created by klaidas on 23/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APTimeZones.h"

@interface TimeManager : NSObject

- (NSString *)timeLeftTillDateTime:(NSDate *)dateTime;
- (NSString *)GMTDateInCurrentTimeZone:(NSDate *)datetime;

@end
