//
//  AppDelegate.h
//  noname
//
//  Created by klaidas on 02/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

