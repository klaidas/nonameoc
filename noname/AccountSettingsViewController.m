//
//  AccountSettingsViewController.m
//  noname
//
//  Created by klaidas on 17/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "AccountSettingsViewController.h"
#import "MainManager.h"

@interface AccountSettingsViewController ()

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
@property (strong, nonatomic) NSUserDefaults *userDefaults;

@end

@implementation AccountSettingsViewController

- (NSUserDefaults *)userDefaults
{
    if (!_userDefaults)
    {
        _userDefaults = [NSUserDefaults standardUserDefaults];
    }
    return _userDefaults;
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([[self.userDefaults objectForKey:@"loggedin"] isEqualToString:@"NO"] || ![self.userDefaults objectForKey:@"loggedin"])
    {
        NSLog(@"loggedOUT");
        [self performLogoutUIChanges];
    }
    else if ([[self.userDefaults objectForKey:@"loggedin"] isEqualToString:@"YES"])
    {
        NSLog(@"loggedIN");
        [self performLoginUIChanges];
    }
}

- (void)performLoginUIChanges
{
    self.usernameTextField.text = [self.userDefaults objectForKey:@"username"];
    self.passwordTextField.text = [self.userDefaults objectForKey:@"password"];
    [self.usernameTextField setEnabled:NO];
    [self.passwordTextField setEnabled:NO];
    [self.loginButton setEnabled:NO];
    [self.logoutButton setEnabled:YES];
}

- (void)performLogoutUIChanges
{
    [self.usernameTextField setEnabled:YES];
    [self.passwordTextField setEnabled:YES];
    [self.loginButton setEnabled:YES];
    [self.logoutButton setEnabled:NO];
}

- (IBAction)loginAction:(UIButton *)sender
{
    if ([self.usernameTextField.text length] > 0 && [self.passwordTextField.text length] > 0)
    {
        NSMutableDictionary *dictionary = [NSMutableDictionary new];
        [dictionary setObject:self.usernameTextField.text forKey:@"username"];
        [dictionary setObject:self.passwordTextField.text forKey:@"password"];
        if ([[MainManager sharedManager] getUserIDForUserInfo:dictionary])
        {
            [self.userDefaults setObject:@"YES" forKey:@"loggedin"];
            [self.userDefaults setObject:self.usernameTextField.text forKey:@"username"];
            [self.userDefaults setObject:self.passwordTextField.text forKey:@"password"];
            [self performLoginUIChanges];
            [[MainManager sharedManager] resetManagedObjectContext];
            //[self.navigationController popToRootViewControllerAnimated:YES];
            [self.tabBarController setSelectedIndex:0];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Login failed, check your username/password."
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];

        }
    }
}

- (IBAction)logoutAction:(UIButton *)sender
{
    [self.userDefaults setObject:@"NO" forKey:@"loggedin"];
    [self.userDefaults removeObjectForKey:@"username"];
    [self.userDefaults removeObjectForKey:@"password"];
    [self performLogoutUIChanges];
}


@end
