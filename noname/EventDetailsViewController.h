//
//  EventDetailsViewController.h
//  noname
//
//  Created by klaidas on 28/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface EventDetailsViewController : UIViewController

@property CLLocationCoordinate2D coordinate;

@end
