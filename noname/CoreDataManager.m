//
//  CoreDataManager.m
//  noname
//
//  Created by klaidas on 15/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "CoreDataManager.h"
#import "Event.h"
#import "Action.h"
#import "User.h"

@implementation CoreDataManager

#pragma mark - init

- (NSManagedObjectContext *)managedObjectContext
{
    if(!_managedObjectContext)
    {
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
        NSManagedObjectModel *mom = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        NSPersistentStoreCoordinator *coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:mom];
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}


#pragma mark - Core Data stuff

- (NSString *)getEventType:(NSNumber *)id
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"id = %@", id]];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    NSString *string = [NSString new];
    if ([array count] == 1)
    {
        Event *event = [array lastObject];
        string = [event type];
        return string;
    }
    else
    {
        NSLog(@"user id error");
    }
    return string;
}

- (NSString *)getUsername:(NSNumber *)id
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"id = %@", id]];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    NSString *string = [NSString new];
    if ([array count] == 1)
    {
        User *user = [array lastObject];
        string = [user username];
        return string;
    }
    else
    {
        NSLog(@"user id error");
    }
    return string;
}

- (BOOL)isEventJoinable:(NSNumber *)eventID
{
    //    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Action"];
    //    [request setPredicate:[NSPredicate predicateWithFormat:@"entity.eventID LIKE %@ ", eventID  ]];
    //    NSArray *array = [self.context executeFetchRequest:request error:nil];
    //
    //    if ([array count] > 0) {
    //        return NO;
    //    }
    //    else
    //    {
    //        return YES;
    //    }
    return NO;
}

- (void)insertActionIntoContext:(NSDictionary *)eventDictionary
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Action"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"id = %@", [NSNumber numberWithInteger:[[eventDictionary valueForKey:@"id"] integerValue]]]];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    if (![array count])
    {
        Action *newAction = [NSEntityDescription insertNewObjectForEntityForName:@"Action" inManagedObjectContext:self.managedObjectContext];
        [newAction setId:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"id"] integerValue]]];
        [newAction setEventID:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"eventID"] integerValue]]];
        [newAction setUserID:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"userID"] integerValue]]];
        [newAction setAction:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"action"] integerValue]]];
    }
}

- (void)insertEventsIntoContext:(NSArray *)eventsArray
{
    for (NSDictionary *eventDictionary in eventsArray)
    {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Event"];
        [request setPredicate:[NSPredicate predicateWithFormat:@"id = %@", [NSNumber numberWithInteger:[[eventDictionary valueForKey:@"id"] integerValue]]]];
        NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
        
        if (![array count])
        {
            Event *newEvent = [NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
            [newEvent setId:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"id"] integerValue]]];
            [newEvent setUserID:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"userID"] integerValue]]];
            [newEvent setComment:[eventDictionary valueForKey:@"comment"]];
            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
           
            [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"];
            [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
            NSLocale * enUSPOSIXLocale = [NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"];
            [dateFormatter setLocale:enUSPOSIXLocale];
            [newEvent setDate:[dateFormatter dateFromString:[eventDictionary objectForKey:@"date"]]];
            
            [newEvent setType:[eventDictionary objectForKey:@"type"]];
            [newEvent setLattitude:[NSNumber numberWithDouble:[[eventDictionary objectForKey:@"latitude"] doubleValue]]];
            [newEvent setLongitude:[NSNumber numberWithDouble:[[eventDictionary objectForKey:@"longitude"] doubleValue]]];
        }

    }
}

- (void)insertUserIntoContext:(NSDictionary *)eventDictionary
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"User"];
    [request setPredicate:[NSPredicate predicateWithFormat:@"id = %@", [NSNumber numberWithInteger:[[eventDictionary valueForKey:@"id"] integerValue]]]];
    NSArray *array = [self.managedObjectContext executeFetchRequest:request error:nil];
    
    if (![array count])
    {
        User *newUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:self.managedObjectContext];
        [newUser setId:[NSNumber numberWithInteger:[[eventDictionary valueForKey:@"id"] integerValue]]];
        [newUser setUsername:[eventDictionary valueForKey:@"username"]];
    }
    
}



- (NSArray *)getActions
{
//    [self fetchActions];
//    [self fetchUsers];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Action"];
    return [self.managedObjectContext executeFetchRequest:request error:nil];
}


@end
