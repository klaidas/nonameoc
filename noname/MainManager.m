//
//  MainManager.m
//  noname
//
//  Created by klaidas on 15/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "MainManager.h"
#import "CoreDataManager.h"
#import "ConnectionManager.h"
#import "Reachability.h"
#import <CoreLocation/CoreLocation.h>

@interface MainManager()<CLLocationManagerDelegate>

@property (strong, nonatomic) CoreDataManager *coreDataManager;
@property (strong, nonatomic) ConnectionManager *connectionManager;
@property (strong, nonatomic) CLLocationManager *locationManager;

@end

@implementation MainManager

#pragma mark - Inits

+ (instancetype)sharedManager
{
    static dispatch_once_t once;
    static MainManager *sharedManager;
    dispatch_once(&once, ^ {
        sharedManager = [[self alloc] init];
        });
    return sharedManager;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setupLocationManager];
    }
    return self;
}

- (CoreDataManager *)coreDataManager
{
    if (!_coreDataManager)
    {
        _coreDataManager = [CoreDataManager new];
    }
    return _coreDataManager;
}

- (ConnectionManager *)connectionManager
{
    if (!_connectionManager)
    {
        _connectionManager = [ConnectionManager new];
    }
    return _connectionManager;
}

- (void)setupLocationManager
{
    _locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    //[self.locationManager requestAlwaysAuthorization];
    [self.locationManager setDistanceFilter:kCLLocationAccuracyHundredMeters];
    [self.locationManager startUpdatingLocation];
}

#pragma mark - Avaivability

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",location.coordinate.latitude] forKey:@"latitude"];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%f",location.coordinate.longitude] forKey:@"longitude"];
}


- (BOOL)internetConnectionAvaivable
{
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        NSLog(@"There IS NO internet connection");
        return NO;
    }
    else
    {
        NSLog(@"There IS internet connection");
        return YES;
    }
}

#pragma mark - User account actions

- (BOOL)submitUserSearchSettings:(NSDictionary *)dictionary
{
    NSMutableDictionary *requestDictionary = [dictionary mutableCopy];
    [requestDictionary setObject:@"5" forKey:@"operation"];
    [requestDictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"username"] forKey:@"username"];
    [requestDictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"password"] forKey:@"password"];
    [requestDictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"userID"] forKey:@"userID"];
    if ([[self.connectionManager performRequestWith:requestDictionary responseIsJson:NO] isEqualToString:@"success"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (int)getUserIDForUserInfo:(NSDictionary *)dictionary
{
    NSMutableDictionary *requestDictionary = [dictionary mutableCopy];
    [requestDictionary setObject:@"7" forKey:@"operation"];
    NSDictionary *responseDictionary = [self.connectionManager performRequestWith:requestDictionary responseIsJson:YES] ;
    return [[NSNumberFormatter new] numberFromString:[responseDictionary objectForKey:@"id"]].intValue;
}

- (void)resetManagedObjectContext
{
    [[self coreDataManager] setManagedObjectContext:nil];
}

#pragma mark - Fetching Data

- (NSArray *)getEvents
{
    [self fetchEventsFromServer];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Event"];
    return [self.coreDataManager.managedObjectContext executeFetchRequest:request error:nil];
}

- (BOOL)fetchEventsFromServer
{
    if (![self internetConnectionAvaivable])
    {
        NSLog(@"Failed to get events, no internet");
        return  NO;
    }
    
    NSDictionary *requestDictionary = [self.connectionManager formRequestForAllEvents];
    if (requestDictionary == nil && [requestDictionary count] == 0)
    {
        NSLog(@"Failed to get events, failed to form request dictionary");
        return  NO;
    }
    
    NSArray *events = [self.connectionManager performRequestWith:requestDictionary responseIsJson:YES];
    if ([events count] > 0)
    {
        [self.coreDataManager insertEventsIntoContext:events];
    }
    NSLog(@"%lu events were fetched from server", [events count]);
    
    return YES;
}

@end

