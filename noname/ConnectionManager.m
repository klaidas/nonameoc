//
//  ConnectionManager.m
//  noname
//
//  Created by klaidas on 15/05/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "ConnectionManager.h"

@interface ConnectionManager()

@property (strong, nonatomic) NSURL *serverURL;

@end

@implementation ConnectionManager

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [self setServerURL:[NSURL URLWithString:@"http://www.ajr.wc.lt/"]];
    }
    return self;
}

#pragma mark - Core

- (id)performRequestWith:(NSDictionary *)dictionary responseIsJson:(BOOL)responseIsJson
{
    NSString *post = [self postRequestFromDictionary:dictionary];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:self.serverURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    if (responseIsJson)
    {
        return [NSJSONSerialization JSONObjectWithData:requestHandler options:0 error:nil];
    }
    else
    {
        return [[NSString alloc] initWithData:requestHandler encoding:NSUTF8StringEncoding];
    }
}


- (NSString *)postRequestFromDictionary:(NSDictionary *)dictionary
{
    __block NSString *string = [NSString new];
    __block  bool first = YES;
    [dictionary enumerateKeysAndObjectsWithOptions:nil usingBlock:^(id key, id obj, BOOL *stop) {
        if (first) {
            first = false;
        }
        else
        {
            string = [string stringByAppendingString:@"&"];
        }
        string = [string stringByAppendingString:[NSString stringWithFormat:@"%@=%@",key,obj]];
    }];
    return string;
}

#pragma mark - Forming requests

- (NSDictionary *)formRequestForAllEvents
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    //    fetch events work- returns json
    [dictionary setObject:@"0" forKey:@"operation"];
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"loggedin"] isEqualToString:@"YES"])
    {
        [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"username"] forKey:@"username"];
        [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"password"] forKey:@"password"];
        [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"userID"] forKey:@"userID"];
        [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"latitude"] forKey:@"latitude"];
        [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"longitude"] forKey:@"longitude"];
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"searchRange"] && [[NSUserDefaults standardUserDefaults] valueForKey:@"searchType"])
        {
            [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"searchRange"] forKey:@"searchRange"];
            [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"searchType"] forKey:@"searchType"];
        }
    }
    return dictionary;
}


- (BOOL)joinEvent:(NSNumber *)eventID
{
    NSMutableDictionary *dictionary = [NSMutableDictionary new];
    [dictionary setObject:@"3" forKey:@"operation"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"username"] forKey:@"username"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"password"] forKey:@"password"];
    [dictionary setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"userID"] forKey:@"userID"];
    [dictionary setObject:[eventID stringValue] forKey:@"eventID"];
    
    NSString *post = [self postRequestFromDictionary:dictionary];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:self.serverURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *string = [[NSString alloc] initWithData:requestHandler encoding:NSUTF8StringEncoding];
    
    if ([string isEqualToString:@"success"])
    {
        return YES;
    }
    return NO;
}

- (BOOL)createEvent:(NSDictionary *)dictionary
{
    NSString *post = [self postRequestFromDictionary:dictionary];
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:self.serverURL];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    NSURLResponse *requestResponse;
    NSData *requestHandler = [NSURLConnection sendSynchronousRequest:request returningResponse:&requestResponse error:nil];
    
    NSString *string = [[NSString alloc] initWithData:requestHandler encoding:NSUTF8StringEncoding];
    
    if ([string isEqualToString:@"success"])
    {
        return YES;
    }
    else
    {
        return NO;
    }
    
}

#pragma mark - Data fetching
//- (void)fetchUsers
//{
//    NSMutableDictionary *dictionary = [NSMutableDictionary new];
//    [dictionary setObject:@"6" forKey:@"operation"];
//    [self fetchData: dictionary];
//}
//
//
//- (void)fetchActions
//{
//    NSMutableDictionary *dictionary = [NSMutableDictionary new];
//    [dictionary setObject:@"4" forKey:@"operation"];
//    [self fetchData: dictionary];
//}

@end
