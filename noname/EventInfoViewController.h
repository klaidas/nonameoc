//
//  EventInfoViewController.h
//  noname
//
//  Created by klaidas on 04/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"

@interface EventInfoViewController : UIViewController

@property (strong, nonatomic) Event *event;

@end
