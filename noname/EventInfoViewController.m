//
//  EventInfoViewController.m
//  noname
//
//  Created by klaidas on 04/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "EventInfoViewController.h"
#import "MyMapView.h"
#import "MainManager.h"
#import "TimeManager.h"

@interface EventInfoViewController ()

@property (weak, nonatomic) IBOutlet MyMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *labelType;
@property (weak, nonatomic) IBOutlet UILabel *labelComment;
@property (weak, nonatomic) IBOutlet UILabel *labelDate;

@property (strong, nonatomic) MainManager *mainManager;
@property (strong, nonatomic) TimeManager *timemanager;

@end

@implementation EventInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mainManager = [MainManager sharedManager];
    self.timemanager = [TimeManager new];
    [self.mapView setMapEvents:@[self.event]];
    [self.mapView changeCameraLocationToMarkerAtIndex:0];
    [self setupInfo];
}

- (void)setupInfo
{
    self.labelType.text = self.event.type;
    self.labelComment.text = self.event.comment;
    self.labelDate.text = [self.timemanager GMTDateInCurrentTimeZone:self.event.date];
}



- (IBAction)shareAction:(UIBarButtonItem *)sender
{
    NSArray *itemsToShare = @[@"linkToWebPage..."];
    UIActivityViewController *activtyVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    [self presentViewController:activtyVC animated:YES completion:nil];
}


@end
