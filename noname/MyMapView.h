//
//  MyMapView.h
//  noname
//
//  Created by klaidas on 03/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>

@interface MyMapView : GMSMapView

@property (strong, nonatomic) NSArray *mapEvents;
@property BOOL locationSelectMode;

- (void)changeCameraLocationToMarkerAtIndex:(NSInteger )index;

@end
