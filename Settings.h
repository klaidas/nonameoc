//
//  Settings.h
//  noname
//
//  Created by klaidas on 26/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Settings : NSManagedObject

@property (nonatomic, retain) NSNumber * searchRange;
@property (nonatomic, retain) NSString * searchType;

@end
