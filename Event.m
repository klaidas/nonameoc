//
//  Event.m
//  noname
//
//  Created by klaidas on 28/04/15.
//  Copyright (c) 2015 klaidas. All rights reserved.
//

#import "Event.h"


@implementation Event

@dynamic comment;
@dynamic userID;
@dynamic date;
@dynamic id;
@dynamic lattitude;
@dynamic longitude;
@dynamic type;
@dynamic participants;

@end
